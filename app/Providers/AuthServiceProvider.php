<?php

namespace App\Providers;

use App\Models\Team;
use App\Policies\TeamPolicy;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Cache;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Team::class => TeamPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Adicionando cache para a consulta do usuario
        Auth::provider('cache-user', function() {
            return resolve(CacheUserProvider::class);
        });
    }
}
