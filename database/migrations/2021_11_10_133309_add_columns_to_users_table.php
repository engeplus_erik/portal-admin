<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->string('apelido')
                ->after('name')
                ->comment('No BD antigo é a coluna usuario');

            $table->boolean('ativo')
                ->after('email')
                ->default(0)
                ->comment('No BD antigo é a coluna status');


            $table->boolean('editor_chefe')
                ->after('current_team_id')
                ->default(0);

            $table->boolean('colunista')
                ->after('editor_chefe')
                ->default(0);

            $table->timestamp('ultimo_acesso')
                ->after('profile_photo_path')
                ->nullable();

            $table->softDeletes()->after('updated_at');

            // criação de indices
            $table->index('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(
                'status',
                'ultimo_acesso',
                'apelido',
                'colunista',
                'editor_chefe',
                'deleted_at'
            );

            $table->dropIndex('email');
        });
    }
}
