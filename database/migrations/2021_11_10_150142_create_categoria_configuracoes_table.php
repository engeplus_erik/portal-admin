<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriaConfiguracoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria_configuracoes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_categoria')->constrained('categorias');
            $table->string('cor', 7)->nullable();
            $table->string('adm_icon', 50)->nullable()->comment('Icone utilizado no painel adm');
            $table->boolean('capa')->default(1);
            $table->boolean('rss')->default(1);
            $table->boolean('middle_slide')->default(0);
            $table->boolean('middle_dia')->default(0);
            $table->tinyInteger('qtd_middle')
                ->default(2)
                ->comment('Quantidade de matérias que podem ser mostradas na barra do meio');

            $table->tinyInteger('coluna_rodape')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoria_configuracoes');
    }
}
