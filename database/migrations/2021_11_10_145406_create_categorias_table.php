<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_categoria')->nullable()->constrained('categorias');
            $table->string('nome', 50);
            $table->string('url', 50);
            $table->boolean('status')->default(0);
            $table->string('view', 50);
            $table->integer('qtd_materia')->default(0);
            $table->tinyInteger('ordem')->default(0);
            $table->integer('qtd_acesso')->default(0);
            $table->integer('qtd_acesso_timeline')->default(0);
            $table->timestamps();

            $table->index('url');

       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias');
    }
}
