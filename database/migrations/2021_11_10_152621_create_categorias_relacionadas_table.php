<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasRelacionadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias_relacionadas', function (Blueprint $table) {
            $table->foreignId('id_categoria')->constrained('categorias');
            $table->foreignId('id_categoria_relacionada')->constrained('categorias');

            $table->primary(['id_categoria', 'id_categoria_relacionada'], 'categorias_relacionadas_id_primary');

            $table->index('id_categoria');
            $table->index('id_categoria_relacionada');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias_relacionadas');
    }
}
