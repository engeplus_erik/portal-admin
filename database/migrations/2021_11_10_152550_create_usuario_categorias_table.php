<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_categorias', function (Blueprint $table) {
            $table->foreignId('id_usuario')->constrained('users');
            $table->foreignId('id_categoria')->constrained('categorias');
            $table->primary(['id_usuario', 'id_categoria']);

            $table->index('id_usuario');
            $table->index('id_categoria');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_categorias');
    }
}
