# Administrativo do portal 

## Instalação

``` bash
# Clonar o  repositório
$ git clone URL portal-admin

# Acessar o novo diretorio
$ cd portal-admin

# Instalar as Dependências do PHP
$ composer install

# Iniciar o docker
$ ./vendor/bin/sail up -d

# Instalar as Dependências do node
$ ./vendor/bin/sail npm install

# COLOCAR A MIGRAÇÃO AQUI
$ 
```
## Uso

``` bash
# 
$ ./vendor/bin/sail npm run dev
```
    
Acessar o sistema em [localhost:81](http://localhost:81)
